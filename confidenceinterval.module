<?php

/**
 * @file
 * An implementation of the Wilson Score Interval to weight feedback.
 */

function confidenceinterval_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/settings/confidenceinterval':
      $output  = t('The aggregate sorting function will attempt to balance the proportion of positive ratings with the uncertainty of a small number of observations. Fortunately, the math for this was worked out in 1927 by Edwin B. Wilson.');
  }
  return $output;
}

/**
 * Implementation of hook_menu().
 */
function confidenceinterval_menu() {
  $items['admin/settings/confidenceinterval'] = array(
    'title' => 'Confidence Interval',
    'description' => 'Configure the statistical power.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('confidenceinterval_admin_settings'),
    'access arguments' => array('administer site configuration'),
  );
  return $items;
}

/**
 * Callback function for admin/settings/confidenceinterval. Display the settings form.
 */
function confidenceinterval_admin_settings() {
  $form = array();

  $stat_power = array(0.05, 0.06, 0.07, 0.08, 0.09, 0.10);
  $form['access']['confidenceinterval_power'] = array(
    '#type' => 'select',
    '#title' => t('Statistical power'),
    '#default_value' => variable_get('confidenceinterval_power', 0.10),
    '#options' => $stat_power,
    '#description' => t('Enter the statistical power level: pick 0.10 to have a 95% chance that your lower bound is correct, 0.05 to have a 97.5% chance, etc'),
  );
  return system_settings_form($form);
}

/*
 * RATING SYSTEM -
 * The rating system will attempt to balance the proportion of positive ratings with the uncertainty of
 * a small number of observations. Fortunately, the math for this was worked out in 1927 by Edwin B. Wilson.
 *
 * @see http://en.wikipedia.org/wiki/Wilson_score_interval#Wilson_score_interval
 *
 * What we want to ask is: Given the ratings I have, there is a 95% chance that the "real" fraction of
 * positive ratings is at least what? Wilson gives the answer. Considering only positive and negative
 * ratings (i.e. not a 5-star scale), the lower bound on the proportion of positive ratings is given by:
 *
 * $z = Rating::confidenceinterval_pnormaldist(1-$power/2,0,1);
 * $p = 1.0 * $positive / $total;
 * ($p + $z * $z / (2 * $total) - $z * sqrt(($p * (1 - $p) + $z * $z / (4 * $total)) / $total)) / (1 + $z * $z / $total);
 *
 *  $positive is the number of positive ratings
 *  $total is the total number of ratings
 *  $power refers to the statistical power: pick 0.10 to have a 95% chance that your lower bound is correct, 0.05 to have a 97.5% chance, etc.
 *
 * @see http://www.derivante.com/2009/09/01/php-content-rating-confidence
 * @see http://www.evanmiller.org/how-not-to-sort-by-average-rating.html
 *
 */

function confidenceinterval_rating_average($positive, $total) {
  // power refers to the statistical power: pick 0.10 to have a 95% chance that your lower bound is correct, 0.05 to have a 97.5% chance, etc.
  $confidenceinterval_power = variable_get('confidenceinterval_power', 0.10);
  if ($total == 0) {
    return 0;
  }
  $z = confidenceinterval_pnormaldist(1 - $confidenceinterval_power / 2, 0, 1);
  $p = 1.0 * $positive / $total;
  $s = ($p + $z * $z / (2 * $total) - $z * sqrt(($p * (1 - $p) + $z * $z / (4 * $total)) / $total)) / (1 + $z * $z / $total);
  return $s;
}

function confidenceinterval_pnormaldist($qn) {
  $b = array(1.570796288, 0.03706987906, -0.8364353589e-3, -0.2250947176e-3, 0.6841218299e-5, 0.5824238515e-5, -0.104527497e-5, 0.8360937017e-7, -0.3231081277e-8, 0.3657763036e-10, 0.6936233982e-12);
  if (0.0 > $qn || 1.0 < $qn) {
    return 0.0;
  }
  if (0.5 == $qn) {
    return 0.0;
  }
  $w1 = $qn;
  if ($qn > 0.5) {
    $w1 = 1.0 - $w1;
  }
  $w3 = -log(4.0 * $w1 * (1.0 - $w1));
  $w1 = $b[0];
  for ($i = 1; $i <= 10; $i++) {
    $w1 += $b[$i] * pow($w3, $i);
  }
  if (0.5 < $qn) {
    return sqrt($w1 * $w3);
  }
  return -sqrt($w1 * $w3);
}

/*
 * Create/Set a new cached vote calculation
 *
 * value_count = Plus vote + Minus vote
 * value_sum   = Plus vote - Minus vote
 *
 * Total Plus vote  = (value_count + value_sum) / 2
 * Total Minus vote = (value_count - value_sum) / 2
 *
 * @see http://drupalcontrib.org/api/function/hook_votingapi_results_alter
 */

/**
 * Implementation of votingapi hook_votingapi_results_alter().
 */
function confidenceinterval_votingapi_results_alter(&$cache, $content_type, $content_id) {
  $sql  = "SELECT v.value_type, v.tag, ";
  $sql .= "COUNT(v.value) as value_count, SUM(v.value) as value_sum  ";
  $sql .= "FROM {votingapi_vote} v ";
  $sql .= "WHERE v.content_type = '%s' AND v.content_id = %d AND v.value_type IN ('points', 'percent') ";
  $sql .= "GROUP BY v.value_type, v.tag";
  $results = db_query($sql, $content_type, $content_id);

  // VotingAPI wants the data in the following format:
  // $cache[$tag][$value_type][$aggregate_function] = $value;
  while ($result = db_fetch_object($results)) {
    $p = ($result->value_count + $result->value_sum) / 2;
    $confidence = confidenceinterval_rating_average($p, $result->value_count);
    if ($result->value_type == 'points') {
      $cache[$result->tag]['points']['confidence'] = $confidence;
    }
  }
}

/**
 * Implementation of hook_votingapi_metadata_alter().
 */
function confidenceinterval_votingapi_metadata_alter(&$data) {
  $data['functions']['confidence'] = array(
    'name' => t('Confidence'),
    'description' => t('Balances the proportion of positive ratings with the uncertainty of a small number of observations'),
    'module' => 'confidenceinterval',
  );
}

