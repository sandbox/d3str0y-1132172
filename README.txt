
Ranks the content by using a binomial proportion confidence interval (what?!).

This module will provide the "confidence" aggregate function to the Voting API
module. Use the name of this function instead of "average" or "count" when
creating Views for modules based on Voting API.

Considering only positive and negative ratings (i.e. not a 5-star scale), the
lower bound on the proportion of positive rating is given by attempting to
balance the proportion of positive ratings with the uncertainty of a small
number of observations.

** Please note this module should only be used with (+1, -1) or 'points' based
modules. 5-star modules use 'percent' to handle calculations.

